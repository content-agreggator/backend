package middlewares

import (
	"context"
	uuid "github.com/satori/go.uuid"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	"gitlab.com/content-agreggator/backend/graph/utils"
	"net"
	"net/http"
	"strings"
)

// UserAuth - user auth middleware structure
type UserAuth struct {
	UserId    uuid.UUID
	Role      dbModels.Role
	IPAddress string
	Token     string
}

var userCtxKey = &contextKey{name: "user"}

type contextKey struct {
	name string
}

// JwtMiddleware middleware for http server
func JwtMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			token := TokenFromHTTPRequestgo(r)
			userID := UserIDFromHTTPRequestgo(token)
			role := UserRoleFromHTTPRequestgo(token)
			ip, _, _ := net.SplitHostPort(r.RemoteAddr)

			userAuth := UserAuth{
				UserId:    userID,
				IPAddress: ip,
				Role:      role,
			}

			// put it in context
			ctx := context.WithValue(r.Context(), userCtxKey, &userAuth)
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

// TokenFromHTTPRequestgo - get jwt token from request
func TokenFromHTTPRequestgo(r *http.Request) string {
	reqToken := r.Header.Get("Authorization")

	var tokenString string
	splitToken := strings.Split(reqToken, "Bearer ")

	if len(splitToken) > 1 {
		tokenString = splitToken[1]
	}
	return tokenString
}

// UserIDFromHTTPRequestgo - get user id from request
func UserIDFromHTTPRequestgo(tokenString string) uuid.UUID {
	token, err := utils.DecodeJwt(tokenString)
	if err != nil {
		return uuid.Nil
	}
	if claims, ok := token.Claims.(*dbModels.UserClaims); ok && token.Valid {
		if claims == nil {
			return uuid.Nil
		}
		return claims.UserID
	}
	return uuid.Nil
}

// UserIDFromHTTPRequestgo - get user id from request
func UserRoleFromHTTPRequestgo(tokenString string) dbModels.Role {
	token, err := utils.DecodeJwt(tokenString)
	if err != nil {
		return dbModels.RoleUndefined
	}
	if claims, ok := token.Claims.(*dbModels.UserClaims); ok && token.Valid {
		if claims == nil {
			return dbModels.RoleUndefined
		}
		return claims.Role
	}
	return dbModels.RoleUndefined
}

// GetAuthFromContext - Gets context
func GetAuthFromContext(ctx context.Context) *UserAuth {
	raw := ctx.Value(userCtxKey)
	if raw == nil {
		return nil
	}

	return raw.(*UserAuth)
}

func (a UserAuth) HasRole(role dbModels.Role) bool {
	if a.Role == role {
		return true
	} else {
		return false
	}
}
