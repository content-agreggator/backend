package middlewares

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/content-agreggator/backend/db"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	"net/http"
)

// JwtMiddleware middleware for http server
func PermissionMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			token := TokenFromHTTPRequestgo(r)

			userID := UserIDFromHTTPRequestgo(token)
			if userID != uuid.Nil {
				user, err := db.DbConnector.User.GetUser(userID)
				if err != nil {
					return
				}
				if user.AccountStatus == dbModels.AccountStatusBlocked {
					return
				}
			}

			next.ServeHTTP(w, r)
		})
	}
}