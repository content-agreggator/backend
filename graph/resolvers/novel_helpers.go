package resolvers

import (
	"gitlab.com/content-agreggator/backend/db"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
	"gitlab.com/content-agreggator/backend/wln"
	wlnModels "gitlab.com/content-agreggator/backend/wln/models"
)

func mapReleases(novel wlnModels.Novel) (out []*dbModels.Release) {
	for _, release := range novel.Releases {
		out = append(out, &dbModels.Release{
			Chapter:   float64(release.Chapter),
			Published: release.Published,
			Srcurl:    release.Srcurl,
			Volume:    float64(release.Volume),
		})

	}
	return
}

func addWlnNovel(id int) (*dbModels.Multimedia, error) {
	novel, err := wln.GetSeriesId(id)
	if err != nil {
		return nil, err
	}



	novelToCreate := &dbModels.Multimedia{
		Demographic: novel.Demographic,
		Description: novel.Description,
		Title:       novel.Title,
		Releases:    mapReleases(*novel),
		Type:        novel.Type,
	}
	novelToCreate.ApiId = novel.Id

	dbNovel, err := db.DbConnector.Multimedia.CreateMultimedia(novelToCreate)
	if err != nil {
		return nil, err
	}

	return dbNovel, nil
}

func mapMultimediaToApi(multimedia dbModels.Multimedia) *apiModels.Multimedia {

	alternateTitles := []*string{}
	for _, name := range multimedia.AlternateNames {
		alternateTitles = append(alternateTitles, &name)
	}

	authors := []*string{}
	for _, author := range multimedia.Authors {
		authors = append(authors, &author.Name)
	}

	genres := []*string{}
	for _, genre := range multimedia.Genres {
		genres = append(genres, &genre.Name)
	}

	return &apiModels.Multimedia{
		ID:              multimedia.Id.String(),
		APIID:           multimedia.ApiId,
		Title:           multimedia.Title,
		AlternateTitles: alternateTitles,
		Authors:         authors,
		Description:     multimedia.Description,
		Genres:          genres,
		Rating:          mapRatingToApi(multimedia.Rating),
		Comments:        mapCommentsToApi(multimedia.Comments),
		//Releases:        mapReleases(multimedia.Releases),
		Type: multimedia.Type,
	}
}

func mapRatingToApi(rating dbModels.Rating) *apiModels.Rating {
	return &apiModels.Rating{
		Avg:   rating.Avg,
		Count: rating.Count,
	}
}

func mapNovelsToApi(novels []dbModels.Multimedia) []*apiModels.Multimedia {
	out := []*apiModels.Multimedia{}
	for _, novel := range novels {
		out = append(out, mapMultimediaToApi(novel))
	}
	return out
}

//func mapReleasesConnectionToApi(releases []*dbModels.Release) *apiModels.ReleasesConnection {
//	var out *apiModels.ReleasesConnection
//
//	return &apiModels.ReleasesConnection{
//		Edges:    nil,
//		PageInfo: nil,
//	}
//}

func mapReleaseToApi(release dbModels.Release) *apiModels.Release {
	return &apiModels.Release{
		ID:        release.Id.String(),
		Chapter:   release.Chapter,
		Published: release.Published,
		Srcurl:    release.Srcurl,
		Volume:    release.Volume,
		Comments:  []*apiModels.Comment{},
	}
}

func mapReleasesConnectionEdge(releases []dbModels.Release) []*apiModels.ReleasesConnectionEdge {
	var out []*apiModels.ReleasesConnectionEdge

	for _, release := range releases {
		out = append(out, &apiModels.ReleasesConnectionEdge{
			Cursor: release.Id.String(),
			Node:   mapReleaseToApi(release),
		})
	}

	return out
}

func mapMultimediasConnectionEdge(novels []dbModels.Multimedia) []*apiModels.MultimediasConnectionEdge {
	var out []*apiModels.MultimediasConnectionEdge

	for _, novel := range novels {
		out = append(out, &apiModels.MultimediasConnectionEdge{
			Cursor: novel.Id.String(),
			Node:   mapMultimediaToApi(novel),
		})
	}

	return out
}
