package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"github.com/99designs/gqlgen/graphql"
	"github.com/vektah/gqlparser/v2/gqlerror"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
	"gitlab.com/content-agreggator/backend/wln"
)

func (r *mutationResolver) AddNovelFromWln(ctx context.Context, id int) (*apiModels.Multimedia, error) {
	loginData, err := wln.LoginToWLN()
	if err != nil {
		return nil, err
	}
	if !wln.IsLogged(loginData["message"]) {
		return nil, gqlerror.Errorf("could not login to wln")
	}

	novel, err := addWlnNovel(id)
	if err != nil {
		return nil, err
	}

	return mapMultimediaToApi(*novel), nil
}

func (r *mutationResolver) AddNovelsFromWln(ctx context.Context, from int, to int) ([]*apiModels.Multimedia, error) {
	var out []*apiModels.Multimedia

	for id := from; id < to; id++ {
		novel, err := addWlnNovel(id)
		if err != nil {
			graphql.AddError(ctx, err)
		}

		out = append(out, mapMultimediaToApi(*novel))
	}

	return out, nil
}
