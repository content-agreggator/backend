package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"github.com/satori/go.uuid"
	"gitlab.com/content-agreggator/backend/db"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	middlewares "gitlab.com/content-agreggator/backend/graph/middleware"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
)

func (r *mutationResolver) AddCommentToRelease(ctx context.Context, draft apiModels.CommentDraft, releaseID string) (*apiModels.Comment, error) {
	auth := middlewares.GetAuthFromContext(ctx)

	releaseId, err := uuid.FromString(releaseID)
	if err != nil {
		return nil, err
	}

	comment, err := db.DbConnector.Comment.AddCommentToRelease(releaseId, &dbModels.Comment{
		UserId: &auth.UserId,
		Text:   draft.Text,
	})
	if err != nil {
		return nil, err
	}

	return mapCommentToApi(comment)
}

func (r *mutationResolver) AddCommentToNovel(ctx context.Context, draft apiModels.CommentDraft, novelID string) (*apiModels.Comment, error) {
	auth := middlewares.GetAuthFromContext(ctx)

	novelId, err := uuid.FromString(novelID)
	if err != nil {
		return nil, err
	}

	comment, err := db.DbConnector.Comment.AddCommentToNovel(novelId, &dbModels.Comment{
		UserId: &auth.UserId,
		Text:   draft.Text,
	})
	if err != nil {
		return nil, err
	}

	return mapCommentToApi(comment)
}

func (r *mutationResolver) AddCommentToComment(ctx context.Context, draft apiModels.CommentDraft, commentID string) (*apiModels.Comment, error) {
	auth := middlewares.GetAuthFromContext(ctx)

	commentId, err := uuid.FromString(commentID)
	if err != nil {
		return nil, err
	}

	comment, err := db.DbConnector.Comment.AddCommentToComment(commentId, &dbModels.Comment{
		UserId: &auth.UserId,
		Text:   draft.Text,
	})
	if err != nil {
		return nil, err
	}

	return mapCommentToApi(comment)
}

func (r *queryResolver) Comments(ctx context.Context, novelID string) ([]*apiModels.Comment, error) {
	novelId, err := uuid.FromString(novelID)
	if err != nil {
		return nil, err
	}

	comments, err := db.DbConnector.Comment.GetComments(novelId)
	if err != nil {
		return nil, err
	}

	return mapCommentsToApi(comments), nil
}
