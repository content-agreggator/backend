package resolvers

import (
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
)

func mapCommentToApi(comment *dbModels.Comment) (*apiModels.Comment, error) {
	return &apiModels.Comment{
		ID:          comment.Id.String(),
		Text:        comment.Text,
		Author:      comment.User.Username,
		CreatedAt:   comment.CreatedAt,
	}, nil
}

func mapCommentsToApi(comments []*dbModels.Comment) []*apiModels.Comment {
	var out []*apiModels.Comment

	for _, comment := range comments {
		com, err := mapCommentToApi(comment)
		if err != nil {
			return nil
		}

		out = append(out, com)
	}

	return out
}
