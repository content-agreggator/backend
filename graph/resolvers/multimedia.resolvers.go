package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/content-agreggator/backend/db"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
)

func (r *mutationResolver) AddMultimedia(ctx context.Context, draft apiModels.NovelDraft) (*apiModels.Multimedia, error) {
	var authors []*dbModels.Author
	for _, author := range draft.Authors {
		authors = append(authors, &dbModels.Author{Name: *author})
	}

	var genres []*dbModels.Genre
	for _, genre := range draft.Genres {
		genres = append(genres, &dbModels.Genre{Name: *genre})
	}

	novelToCreate := &dbModels.Multimedia{
		Description: draft.Description,
		Title:       draft.Title,
		Authors:     authors,
		Genres:      genres,
	}

	dbNovel, err := db.DbConnector.Multimedia.CreateMultimedia(novelToCreate)
	if err != nil {
		return nil, err
	}

	return mapMultimediaToApi(*dbNovel), nil
}

func (r *mutationResolver) RemoveMultimedia(ctx context.Context, multimediaID string) (*apiModels.Multimedia, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *mutationResolver) EditMultimedia(ctx context.Context, multimediaID string, draft apiModels.EditMultimediaDraft) (*apiModels.Multimedia, error) {
	logrus.Info("draft 1 ", draft)

	multimediId, err := uuid.FromString(multimediaID)
	if err != nil {
		return nil, err
	}

	var authors []*dbModels.Author
	for _, author := range draft.Authors {
		authors = append(authors, &dbModels.Author{Name: *author})
	}

	var genres []*dbModels.Genre
	for _, genre := range draft.Genres {
		genres = append(genres, &dbModels.Genre{Name: *genre})
	}

	multimedia, err := db.DbConnector.Multimedia.UpdateMultimedia(multimediId, &dbModels.Multimedia{
		AlternateNames: draft.AlternateTitles,
		Authors:        authors,
		Description:    draft.Description,
		Title:          draft.Title,
		Genres:         genres,
		Type:           draft.Type,
	})
	if err != nil {
		return nil, err
	}

	return mapMultimediaToApi(*multimedia), nil
}

func (r *mutationResolver) RefreshMultimedias(ctx context.Context) ([]*apiModels.Multimedia, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Multimedia(ctx context.Context, id string) (*apiModels.Multimedia, error) {
	novelId, err := uuid.FromString(id)
	if err != nil {
		return nil, err
	}

	novel, err := db.DbConnector.Multimedia.GetMultimedia(novelId)
	if err != nil {
		return nil, err
	}

	return mapMultimediaToApi(*novel), nil
}

func (r *queryResolver) MultimediasConnection(ctx context.Context, after *string, before *string, first *int, last *int) (*apiModels.MultimediasConnection, error) {
	logrus.Info("MultimediasConnection args: %v %v %v %v", after, before, first,last)

	novels, pageInfo, err := db.DbConnector.Multimedia.GetMultimedias(after, before, first, last)
	if err != nil {
		return nil, err
	}

	return &apiModels.MultimediasConnection{

		Edges: mapMultimediasConnectionEdge(*novels),

		PageInfo: pageInfo,
	}, nil
}

func (r *queryResolver) ReleasesConnection(ctx context.Context, novelID string, after *string, before *string, first *int, last *int) (*apiModels.ReleasesConnection, error) {
	novelId, err := uuid.FromString(novelID)
	if err != nil {
		return nil, err
	}

	releases, pageInfo, err := db.DbConnector.Release.GetReleases(novelId, after, before, first, last)
	if err != nil {
		return nil, err
	}

	return &apiModels.ReleasesConnection{

		Edges: mapReleasesConnectionEdge(*releases),

		PageInfo: pageInfo,
	}, nil
}
