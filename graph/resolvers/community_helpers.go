package resolvers

import (
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
)

func mapCommunitiesConnectionEdge(communities []dbModels.Community) []*apiModels.CommunitiesConnectionEdge {
	var out []*apiModels.CommunitiesConnectionEdge

	for _, community := range communities {
		out = append(out, &apiModels.CommunitiesConnectionEdge{
			Cursor: community.Id.String(),
			Node:   mapCommunityToApi(&community),
		})
	}
	return out
}

func mapCommunityToApi(community *dbModels.Community) *apiModels.Community {
	users := []*apiModels.UserCommunity{}
	if community.Users != nil {
		users = mapUserCommunitiesToApi(community.Users)
	}

	out := &apiModels.Community{
		ID:          community.Id.String(),
		Name:        community.Name,
		Description: community.Description,
		Users:       users,
	}

	return out
}

func mapUserCommunityToApi(uc *dbModels.UserCommunity) *apiModels.UserCommunity {
	return &apiModels.UserCommunity{
		UserID:      uc.UserId.String(),
		CommunityID: uc.CommunityId.String(),
		UserName:    uc.User.Username,
		Role:        string(uc.Role),
	}
}

func mapUserCommunitiesToApi(ucs []*dbModels.UserCommunity) []*apiModels.UserCommunity {
	var out []*apiModels.UserCommunity

	for _, uc := range ucs {
		out = append(out, mapUserCommunityToApi(uc))
	}

	return out
}
