package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"net/mail"
	"time"

	"github.com/satori/go.uuid"
	"github.com/vektah/gqlparser/v2/gqlerror"
	"gitlab.com/content-agreggator/backend/db"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
	"gitlab.com/content-agreggator/backend/graph/utils"
)

func (r *mutationResolver) Login(ctx context.Context, email string, password string) (*apiModels.Auth, error) {
	_, err := mail.ParseAddress(email)
	if err != nil {
		return nil, err
	}

	user, err := db.DbConnector.User.GetUserByEmail(email)
	if err != nil {
		return nil, err
	}

	if !utils.ComparePassword(password, user.Password) {
		return nil, gqlerror.Errorf("passwords does not match")
	}

	expiredAt := int(time.Now().Add(time.Hour * 1).Unix())

	return &apiModels.Auth{
		Token:     utils.GenerateJwt(uuid.FromStringOrNil(user.Id.String()), int64(expiredAt), user.Role),
		ExpiredAt: expiredAt,
	}, nil
}

func (r *mutationResolver) Register(ctx context.Context, input apiModels.RegisterInput) (*apiModels.UserSelf, error) {
	passwordHash, err := utils.HashPassword(input.Password)
	if err != nil {
		return nil, err
	}

	user, err := db.DbConnector.User.CreateUser(&dbModels.User{
		Username: input.Username,
		Password: passwordHash,
		Email:    input.Email,
	})

	if err != nil {
		return nil, gqlerror.Errorf("can't create user; %w", err)
	}

	return &apiModels.UserSelf{
		ID:       user.Id.String(),
		Username: user.Username,
		Password: user.Password,
		Email:    user.Email,
		Friends:  nil,
		Role:     dbModels.RoleUser,
	}, nil
}
