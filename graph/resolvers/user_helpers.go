package resolvers

import (
	"github.com/sirupsen/logrus"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
)

func mapUserOtherToApi(user *dbModels.User) *apiModels.UserOther {
	logrus.Info(11111, user.Friends)

	friends := []*apiModels.UserOther{}
	for _, friend := range user.Friends {
		friends = append(friends, &apiModels.UserOther{
			Username: friend.Username,
			Role:     string(friend.Role),
			Friends:  nil,
		})
	}

	return &apiModels.UserOther{
		ID:       user.Id.String(),
		Username: user.Username,
		Role:     string(user.Role),
		Friends:  friends,
		Status:   string(user.AccountStatus),
	}
}

func mapUserOthersToApi(users []*dbModels.User) []*apiModels.UserOther {
	var out []*apiModels.UserOther

	for _, user := range users {
		out = append(out, mapUserOtherToApi(user))
	}
	return out
}
