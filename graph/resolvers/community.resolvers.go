package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/content-agreggator/backend/db"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	middlewares "gitlab.com/content-agreggator/backend/graph/middleware"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
)

func (r *mutationResolver) AddCommunity(ctx context.Context, draft apiModels.CommunityDraft) (bool, error) {
	auth := middlewares.GetAuthFromContext(ctx)

	_, err := db.DbConnector.Community.CreateCommunity(&dbModels.Community{
		Name:        draft.Title,
		Description: draft.Description,
		Users: []*dbModels.UserCommunity{
			{
				UserId: &auth.UserId,
				Role:   dbModels.CommunityRoleAdmin,
			},
		},
	})
	if err != nil {
		return false, err
	}

	return true, nil
}

func (r *mutationResolver) JoinCommunity(ctx context.Context, communityID string) (*apiModels.UserCommunity, error) {
	auth := middlewares.GetAuthFromContext(ctx)

	communityId, err := uuid.FromString(communityID)
	if err != nil {
		return nil, err
	}

	userCommunity, err := db.DbConnector.Community.AddUserToCommunity(auth.UserId, communityId)
	if err != nil {
		return nil, err
	}

	return mapUserCommunityToApi(userCommunity), nil
}

func (r *mutationResolver) LeaveCommunity(ctx context.Context, communityID string) (*apiModels.UserCommunity, error) {
	auth := middlewares.GetAuthFromContext(ctx)

	communityId, err := uuid.FromString(communityID)
	if err != nil {
		return nil, err
	}

	userCommunity, err := db.DbConnector.Community.RemoveUserFromCommunity(auth.UserId, communityId)
	if err != nil {
		return nil, err
	}

	return mapUserCommunityToApi(userCommunity), nil
}

func (r *queryResolver) Community(ctx context.Context, id string) (*apiModels.Community, error) {
	communityId, err := uuid.FromString(id)
	if err != nil {
		return nil, err
	}
	community, err := db.DbConnector.Community.GetCommunity(communityId)
	if err != nil {
		return nil, err
	}

	return mapCommunityToApi(community), nil
}

func (r *queryResolver) CommunitiesConnection(ctx context.Context, after *string, before *string, first *int, last *int) (*apiModels.CommunitiesConnection, error) {
	communities, pageInfo, err := db.DbConnector.Community.GetCommunities(after, before, first, last)
	if err != nil {
		return nil, err
	}

	logrus.Info(communities, *communities)
	return &apiModels.CommunitiesConnection{
		Edges:    mapCommunitiesConnectionEdge(*communities),
		PageInfo: pageInfo,
	}, nil
}

func (r *queryResolver) IsUserInCommunity(ctx context.Context, communityID string) (bool, error) {
	auth := middlewares.GetAuthFromContext(ctx)

	communityId, err := uuid.FromString(communityID)
	if err != nil {
		return false, err
	}

	isInCommunity, err := db.DbConnector.Community.IsUserInCommunity(auth.UserId, communityId)
	if err != nil {
		return false, err
	}

	return isInCommunity, nil
}
