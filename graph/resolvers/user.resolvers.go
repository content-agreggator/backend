package resolvers

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/content-agreggator/backend/db"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	middlewares "gitlab.com/content-agreggator/backend/graph/middleware"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
)

func (r *mutationResolver) BanUser(ctx context.Context, userID string) (bool, error) {
	auth := middlewares.GetAuthFromContext(ctx)
	if auth.Role != dbModels.RoleAdmin {
		return false, fmt.Errorf("no permission to ban user")
	}

	userId, err := uuid.FromString(userID)

	_, err = db.DbConnector.User.ChangeUserAccountStatus(userId, dbModels.AccountStatusBlocked)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (r *mutationResolver) UnbanUser(ctx context.Context, userID string) (bool, error) {
	auth := middlewares.GetAuthFromContext(ctx)
	if auth.Role != dbModels.RoleAdmin {
		return false, fmt.Errorf("no permission to ban user")
	}

	userId, err := uuid.FromString(userID)

	_, err = db.DbConnector.User.ChangeUserAccountStatus(userId, dbModels.AccountStatusActive)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (r *mutationResolver) RemoveUserAccount(ctx context.Context, userID string) (*apiModels.UserOther, error) {
	userId, err := uuid.FromString(userID)
	if err != nil {
		return nil, err
	}

	auth := middlewares.GetAuthFromContext(ctx)
	if auth.Role != dbModels.RoleAdmin || auth.UserId == userId {
		return nil, fmt.Errorf("no permission to remove user")
	}

	user, err := db.DbConnector.User.DeleteUser(userId)
	if err != nil {
		return nil, err
	}

	return mapUserOtherToApi(user), nil
}

func (r *mutationResolver) AddFriend(ctx context.Context, friendID string) (bool, error) {
	friendId, err := uuid.FromString(friendID)
	if err != nil {
		return false, err
	}

	auth := middlewares.GetAuthFromContext(ctx)

	err = db.DbConnector.User.AddFriend(auth.UserId, friendId)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (r *mutationResolver) RemoveFriend(ctx context.Context, friendID string) (bool, error) {
	friendId, err := uuid.FromString(friendID)
	if err != nil {
		return false, err
	}

	auth := middlewares.GetAuthFromContext(ctx)

	err = db.DbConnector.User.RemoveFriend(auth.UserId, friendId)
	if err != nil {
		return false, err
	}

	return true, nil
}

func (r *queryResolver) User(ctx context.Context) (*apiModels.UserSelf, error) {
	auth := middlewares.GetAuthFromContext(ctx)
	logrus.Info("auth", auth)
	user, err := db.DbConnector.User.GetUser(auth.UserId)
	if err != nil {
		return nil, err
	}
	logrus.Info(2, user)

	return &apiModels.UserSelf{
		ID:       user.Id.String(),
		Username: user.Username,
		Password: user.Password,
		Email:    user.Email,
		Friends:  nil,
		Role:     string(user.Role),
		Status:   string(user.AccountStatus),
	}, nil
}

func (r *queryResolver) UserOther(ctx context.Context, id string) (*apiModels.UserOther, error) {
	userId, err := uuid.FromString(id)
	if err != nil {
		return nil, err
	}

	user, err := db.DbConnector.User.GetUser(userId)
	if err != nil {
		return nil, err
	}

	logrus.Info(22, user)

	return mapUserOtherToApi(user), nil
}

func (r *queryResolver) IsUserAFriend(ctx context.Context, userID string) (bool, error) {
	userId, err := uuid.FromString(userID)
	if err != nil {
		return false, err
	}

	auth := middlewares.GetAuthFromContext(ctx)

	logrus.Info(1, auth.UserId, userId)

	isFriend, err := db.DbConnector.User.IsUserAFriend(auth.UserId, userId)
	if err != nil {
		return false, err
	}

	return isFriend, nil
}
