package utils

import (
	"github.com/dgrijalva/jwt-go"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/content-agreggator/backend/db/models"
)

var issuer = []byte("gitlab/piotrkowalski")

// DecodeJwt decode jwt
func DecodeJwt(token string) (*jwt.Token, error) {
	return jwt.ParseWithClaims(token, &models.UserClaims{}, func(token *jwt.Token) (interface{}, error) {
		return issuer, nil
	})
}

// GenerateJwt create jwt
func GenerateJwt(userID uuid.UUID, expiredAt int64, role models.Role) string {
	claims := models.UserClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiredAt,
			Issuer:    string(issuer),
		},
		UserID: userID,
		Role:   role,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, _ := token.SignedString(issuer)

	return signedToken
}