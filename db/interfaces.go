package db

import (
	uuid "github.com/satori/go.uuid"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
)

type DaoGatherer struct {
	User       UserDao
	Multimedia MultimediaDao
	Comment    CommentDao
	Release    ReleaseDao
	Community  CommunityDao
}

var DbConnector *DaoGatherer

type UserDao interface {
	CreateUser(user *dbModels.User) (*dbModels.User, error)
	GetUser(id uuid.UUID) (*dbModels.User, error)
	GetUserByEmail(email string) (*dbModels.User, error)

	ChangeUserAccountStatus(userId uuid.UUID, status dbModels.AccountStatus) (*dbModels.User, error)
	AddFriend(userId, friendId uuid.UUID) error
	RemoveFriend(useris, friendId uuid.UUID) error

	DeleteUser(id uuid.UUID) (*dbModels.User, error)

	IsUserAFriend(userId, friendId uuid.UUID) (bool, error)
}

type MultimediaDao interface {
	CreateMultimedia(novel *dbModels.Multimedia) (*dbModels.Multimedia, error)
	GetMultimedia(id uuid.UUID) (*dbModels.Multimedia, error)
	GetMultimedias(after *string, before *string, first *int, last *int) (*[]dbModels.Multimedia, *apiModels.PageInfo, error)

	UpdateMultimedia(multimediaID uuid.UUID, updateModel *dbModels.Multimedia) (*dbModels.Multimedia, error)
	RemoveMultimedia(id uuid.UUID) error
}

type CommentDao interface {
	AddCommentToNovel(novelId uuid.UUID, comment *dbModels.Comment) (*dbModels.Comment, error)
	AddCommentToRelease(releaseId uuid.UUID, comment *dbModels.Comment) (*dbModels.Comment, error)
	AddCommentToComment(parentId uuid.UUID, comment *dbModels.Comment) (*dbModels.Comment, error)
	GetComments(novelId uuid.UUID) ([]*dbModels.Comment, error)
}

type ReleaseDao interface {
	GetReleases(novelId uuid.UUID, after *string, before *string, first *int, last *int) (*[]dbModels.Release, *apiModels.PageInfo, error)
}

type CommunityDao interface {
	CreateCommunity(community *dbModels.Community) (*dbModels.Community, error)
	GetCommunity(id uuid.UUID) (*dbModels.Community, error)
	GetCommunities(after *string, before *string, first *int, last *int) (*[]dbModels.Community, *apiModels.PageInfo, error)

	AddUserToCommunity(userId, communityId uuid.UUID) (*dbModels.UserCommunity, error)
	RemoveUserFromCommunity(userId, communityId uuid.UUID) (*dbModels.UserCommunity, error)
	ChangeUserCommunityRole(userId, communityId uuid.UUID, role dbModels.CommunityRole) (*dbModels.UserCommunity, error)
	IsUserInCommunity(userId, communityId uuid.UUID) (bool, error)
}
