package mock

import (
	"github.com/sirupsen/logrus"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	"gitlab.com/content-agreggator/backend/graph/utils"
	"gorm.io/gorm"
)

func CreateMockData(db *gorm.DB) error {
	if err := createAdmin(db); err != nil {
		return err
	}
	if err := createUser(db); err != nil {
		return err
	}
	if err := createExampleCommunity(db); err != nil {
		return err
	}



	return nil
}

func createAdmin(db *gorm.DB) error {
	passwordHash, err := utils.HashPassword("admin")
	if err != nil {
		return err
	}

	if err = db.Create(&dbModels.User{
		Username: "TestAdmin",
		Password: passwordHash,
		Email:    "admin@admin.com",
		Role:     dbModels.RoleAdmin,
	}).Error; err != nil {
		logrus.Info("admin already created")
		return err
	}
	return nil
}

func createUser(db *gorm.DB) error {
	passwordHash, err := utils.HashPassword("user")
	if err != nil {
		return err
	}

	if err = db.Create(&dbModels.User{
		Username: "TestUser",
		Password: passwordHash,
		Email:    "user@user.com",
		Role:     dbModels.RoleUser,
	}).Error; err != nil {
		logrus.Info("user already created")
		return err
	}
	return nil
}

func createExampleCommunity(db *gorm.DB) error {
	err := db.Create(&dbModels.Community{
		Name:        "Example Community Title",
		Description: "Example Community Description",
	}).Error
	if err != nil {
		return err
	}
	return nil
}