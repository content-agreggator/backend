package db

import (
	"gitlab.com/content-agreggator/backend/db/mock"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

func ConnectDb() error {
	dsn := "host=192.168.1.235 user=postgres password=postgres dbname=novelservice port=5432 sslmode=disable TimeZone=Asia/Shanghai"
	tx, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return err
	}
	db = tx
	return nil
}

func MigrateDb() error {
	migrateModels := []interface{}{
		dbModels.Multimedia{},
		dbModels.Title{},
		dbModels.Author{},
		dbModels.Genre{},
		dbModels.User{},
		dbModels.Release{},
		dbModels.Comment{},
		dbModels.Community{},
		dbModels.Rating{},
		dbModels.UserCommunity{},
	}

	err := db.Migrator().DropTable(append([]interface{}{
		"multimedia_authors",
		"multimedia_genres",
		"user_friends",
		"user_communities",
		"user_comments",
	},
	migrateModels...)...)
	if err != nil {
		return err
	}

	err = db.Migrator().AutoMigrate(migrateModels...)
	if err != nil {
		return err
	}

	if err = mock.CreateMockData(db); err != nil {
		return err
	}


	return nil
}

func GetDb() *gorm.DB {
	return db
}



