package models

import uuid "github.com/satori/go.uuid"

type Community struct {
	Base

	Name        string `gorm:"unique"`
	Description string
	Users       []*UserCommunity `gorm:"foreignKey:CommunityId"`
	Comments    []*Comment
}

type UserCommunity struct {
	Base

	CommunityId *uuid.UUID
	Community   *Community

	UserId *uuid.UUID
	User   *User

	Role CommunityRole
}

type CommunityRole string

const (
	CommunityRoleUndefined CommunityRole = "Undefined"
	CommunityRoleAdmin                   = "Admin"
	CommunityRoleUser                    = "User"
)
