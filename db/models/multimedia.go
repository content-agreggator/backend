package models

import (
	"github.com/lib/pq"
	uuid "github.com/satori/go.uuid"
)

type Release struct {
	Base
	MultimediaId uuid.UUID
	Chapter      float64
	Published    string
	Srcurl       string
	Volume       float64
	Comments     []Comment
}

type Multimedia struct {
	Base
	ApiId     int

	AlternateNames pq.StringArray `gorm:"type:text[]"`
	Authors        []*Author      `gorm:"many2many:multimedia_authors;"`
	Demographic    string
	Description    string

	Title string

	Genres   []*Genre `gorm:"many2many:multimedia_genres;"`
	Releases []*Release

	Comments []*Comment
	Type     string
	Rating   Rating
}

type Title struct {
	MultimediaId uuid.UUID
	Name         string
}

type Author struct {
	Base
	Name       string        `gorm:"unique"`
	Multimedia []*Multimedia `gorm:"many2many:multimedia_authors;"`
}

type Genre struct {
	Base
	Name       string        `gorm:"unique"`
	Multimedia []*Multimedia `gorm:"many2many:multimedia_genres;"`
}

type Rating struct {
	Base
	MultimediaId uuid.UUID
	Avg          float64
	Count        int
}
