package models

import (
	"github.com/dgrijalva/jwt-go"
	uuid "github.com/satori/go.uuid"
)

type User struct {
	Base
	Username      string
	Password      string
	Email         string  `gorm:"index:unique"`
	Friends       []*User `gorm:"many2many:user_friends"`
	Role          Role
	Communities   []*UserCommunity `gorm:"foreignKey:UserId"`
	AccountStatus AccountStatus
	Comments      []*Comment
}

type Role string

const (
	RoleUndefined Role = "Undefined"
	RoleAdmin          = "Admin"
	RoleUser           = "User"
)

type AccountStatus string

const (
	AccountStatusUndefined AccountStatus = "Undefined"
	AccountStatusActive                  = "Active"
	AccountStatusBlocked                 = "Blocked"
)

// UserClaims for jwt
type UserClaims struct {
	UserID uuid.UUID `json:"user_id"`
	Role   Role
	jwt.StandardClaims
}
