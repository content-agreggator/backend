package models

import uuid "github.com/satori/go.uuid"

type Comment struct {
	Base
	SubComments []*Comment `gorm:"foreignkey:ParentId"`
	UserId      *uuid.UUID
	User        User
	Text        string

	ParentId    *uuid.UUID
	ReleaseId   *uuid.UUID
	MultimediaId     *uuid.UUID
	CommunityId *uuid.UUID
}
