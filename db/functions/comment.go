package functions

import (
	uuid "github.com/satori/go.uuid"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Comment struct {
	Db *gorm.DB
}

func (c Comment) AddCommentToNovel(novelId uuid.UUID, comment *dbModels.Comment) (*dbModels.Comment, error) {
	if err := c.Db.First(&dbModels.Multimedia{}, novelId).Association("Comments").Append(comment); err != nil {
		return nil, err
	}

	return comment, nil
}

func (c Comment) AddCommentToRelease(releaseId uuid.UUID, comment *dbModels.Comment) (*dbModels.Comment, error) {
	if err := c.Db.First(&dbModels.Release{}, releaseId).Association("Comments").Append(comment); err != nil {
		return nil, err
	}

	return comment, nil
}

func (c Comment) AddCommentToComment(parentId uuid.UUID, comment *dbModels.Comment) (*dbModels.Comment, error) {
	if err := c.Db.First(&dbModels.Comment{}, parentId).Association("SubComments").Append(comment); err != nil {
		return nil, err
	}

	return comment, nil
}

func (c Comment) GetComments(novelId uuid.UUID) ([]*dbModels.Comment, error) {
	novel := &dbModels.Multimedia{}
	novel.Id = novelId

	var comments []*dbModels.Comment

	err := c.Db.Model(novel).Preload(clause.Associations).Association("Comments").Find(&comments)
	if err != nil {
		return nil, err
	}

	return comments,nil
}
