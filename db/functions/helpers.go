package functions

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
	"gorm.io/gorm"
)

func customCursorPaginator(after *string, before *string, first *int, last *int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		db = db.Order("id asc")


		if after != nil {
			db = db.Where("id > ?", *after)

		}

		if before != nil {
			db =  db.Where("id < ? ", *before)
		}

		if first != nil {
			if *first < 0 {
				db.AddError( fmt.Errorf("first is less than 0"))
			}
			db =  db.Limit(*first)
		}

		if last != nil {
			if *last < 0 {
				db.AddError( fmt.Errorf("last is less than 0"))
			}
			db =  db.Limit(*last)
		}

		if first == nil && last == nil {
			db = db.Limit(20)
		}

		return db
	}
}
func getPageInfo(db *gorm.DB, ids []string, model interface{}) (*apiModels.PageInfo, error) {
	if len(ids) > 0 {
		pageInfo := &apiModels.PageInfo{
			StartCursor: ids[0],
			EndCursor:   ids[len(ids)-1],
		}

		var count int64
		err := db.Model(model).Where("id >= ?", ids[len(ids)-1]).Count(&count).Error
		if err != nil {
			return nil, err
		}

		if count > 1 {
			pageInfo.HasNextPage = true
		}

		err = db.Model(model).Where("id <= ?", ids[0]).Count(&count).Error
		if err != nil {
			return nil, err
		}
		if count > 1 {
			pageInfo.HasPreviousPage = true
		}

		return pageInfo, nil
	}

	return &apiModels.PageInfo{
		StartCursor:     uuid.Nil.String(),
		EndCursor:       uuid.Nil.String(),
		HasNextPage:     false,
		HasPreviousPage: false,
	}, nil

}

//
//
//	return &apiModels.PageInfo{
//		HasPreviousPage: hasPreviousPage,
//		HasNextPage:     hasNextPage,
//		TotalCount:      int(totalCount),
//		Page:            input.Page,
//	}
//}

//func paginate(input *apiModels.PageInput) func(db *gorm.DB) *gorm.DB {
//	return func (db *gorm.DB) *gorm.DB {
//		if input == nil {
//			input = &apiModels.PageInput{
//				Limit: 10,
//				Page:  1,
//			}
//		}
//
//
//
//		pageSize := input.Limit
//		switch {
//		case pageSize > 100:
//			pageSize = 100
//		case pageSize <= 0:
//			pageSize = 10
//		}
//
//		offset := (input.Page - 1) * pageSize
//		return db.Offset(offset).Limit(pageSize)
//	}
//}
