package functions

import (
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
	"gorm.io/gorm"
)

type Release struct {
	Db *gorm.DB
}

func (r Release) GetReleases(novelId uuid.UUID, after *string, before *string, first *int, last *int) (*[]dbModels.Release, *apiModels.PageInfo, error) {
	releases := []dbModels.Release{}

	if err := r.Db.Where("multimedia_Id = ?", novelId).Scopes(customCursorPaginator(after, before, first, last)).Find(&releases).Error; err != nil {
		return nil, nil, err
	}

	var ids []string
	for _, novel := range releases {
		ids = append(ids, novel.Id.String())
	}

	info, err := getPageInfo(r.Db, ids, dbModels.Multimedia{})
	if err != nil {
		return nil, nil, err
	}

	logrus.Info("Release list: %v", releases)

	return &releases, info, nil
}
