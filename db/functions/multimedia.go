package functions

import (
	"errors"
	"fmt"
	uuid "github.com/satori/go.uuid"
	"github.com/vektah/gqlparser/v2/gqlerror"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"strings"
)

type Multimedia struct {
	Db *gorm.DB
}

func (n Multimedia) CreateMultimedia(novel *dbModels.Multimedia) (*dbModels.Multimedia, error) {
	var preparedNovel *dbModels.Multimedia

	err := n.Db.Transaction(func(tx *gorm.DB) error {
		preparedNovel = &dbModels.Multimedia{
			Demographic: novel.Demographic,
			Description: novel.Description,
			Title:       novel.Title,
			Genres:      nil,
			Type:        novel.Type,
			Releases:    novel.Releases,
		}

		var authors []*dbModels.Author
		for _, author := range novel.Authors {
			err := tx.First(author, "name = ?", author.Name).Error
			if errors.Is(err, gorm.ErrRecordNotFound) {
				errb := tx.Create(author).Error
				if errb != nil {
					return errb
				}
			} else {
				return err
			}

			authors = append(authors, author)
		}
		preparedNovel.Authors = authors

		var genres []*dbModels.Genre
		for _, genre := range novel.Genres {
			err := tx.First(genre, "name = ?", genre.Name).Error
			if errors.Is(err, gorm.ErrRecordNotFound) {
				errb := tx.Create(genre).Error
				if errb != nil {
					return errb
				}
			} else {
				return err
			}

			genres = append(genres, genre)
		}
		preparedNovel.Genres = genres

		err := tx.Where("Name = ?", "api_id").
			Create(preparedNovel).Error
		if err != nil {
			return gqlerror.Errorf("novel with api id= %v already exists", novel.ApiId)
		}

		// return nil will commit the whole transaction
		return nil
	})
	if err != nil {
		return nil, err
	}

	return preparedNovel, nil
}

func (n Multimedia) GetMultimedia(id uuid.UUID) (*dbModels.Multimedia, error) {

	db := n.Db.Preload("Comments").Preload("Genres")
	for i := 0; i < 20; i++ {
		db = db.Preload(fmt.Sprintf("Comments%v", strings.Repeat(".SubComments", i)))
	}

	novel := &dbModels.Multimedia{}
	novel.Id = id
	if err := db.Preload(clause.Associations).First(novel).Error; err != nil {
		return nil, err
	}
	return novel, nil
}

func (n Multimedia) GetMultimedias(after *string, before *string, first *int, last *int) (*[]dbModels.Multimedia, *apiModels.PageInfo, error) {
	novels := []dbModels.Multimedia{}

	if err := n.Db.Scopes(customCursorPaginator(after, before, first, last)).Find(&novels).Error; err != nil {
		return nil, nil, err
	}

	var ids []string
	for _, novel := range novels {
		ids = append(ids, novel.Id.String())
	}

	info, err := getPageInfo(n.Db, ids, dbModels.Multimedia{})
	if err != nil {
		return nil, nil, err
	}

	return &novels, info, nil
}

func (n Multimedia) RemoveMultimedia(id uuid.UUID) error {
	panic("implement me")
}

func (n Multimedia) UpdateMultimedia(multimediaID uuid.UUID, updateModel *dbModels.Multimedia) (*dbModels.Multimedia, error) {
	var multimediaToUpdate *dbModels.Multimedia

	err := n.Db.Transaction(func(tx *gorm.DB) error {
		multimediaToUpdate = &dbModels.Multimedia{
			AlternateNames: updateModel.AlternateNames,
			Authors:        nil,
			Description:    updateModel.Description,
			Title:          updateModel.Title,
			Genres:         nil,
			Type:           updateModel.Type,
		}

		multimedia := &dbModels.Multimedia{}
		multimedia.Id = multimediaID
		err := tx.Model(multimedia).Updates(multimediaToUpdate).Error
		if err != nil {
			return err
		}

		err = tx.Model(multimedia).Association("Authors").Clear()
		if err != nil {
			return err
		}
		for _, author := range updateModel.Authors {
			err = tx.Debug().Where("name = ?", author.Name).FirstOrCreate(author).Error
			if err != nil {
				return err
			}

			err = tx.Debug().Omit("Authors.*").First(multimedia).Association("Authors").Append(&dbModels.Author{Base:dbModels.Base{Id: author.Id}})
			if err != nil {
				return err
			}

		}

		err = tx.Model(multimedia).Association("Genres").Clear()
		if err != nil {
			return err
		}
		for _, genre := range updateModel.Genres {
			err = tx.Debug().Where("name = ?", genre.Name).FirstOrCreate(genre).Error
			if err != nil {
				return err
			}

			err = tx.Debug().Omit("Genres.*").First(multimedia).Association("Genres").Append(&dbModels.Genre{Base:dbModels.Base{Id: genre.Id}})
			if err != nil {
				return err
			}

		}

		// return nil will commit the whole transaction
		return nil
	})
	if err != nil {
		return nil, err
	}

	multimedia := &dbModels.Multimedia{}
	err = n.Db.First(multimedia, multimediaID).Error
	if err != nil {
		return nil, err
	}

	return multimediaToUpdate, nil
}
