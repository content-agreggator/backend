package functions

import (
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type User struct {
	Db *gorm.DB
}

func (u User) CreateUser(user *dbModels.User) (*dbModels.User, error) {
	if err := u.Db.Create(user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (u User) GetUser(id uuid.UUID) (*dbModels.User, error) {
	user := dbModels.User{}

	if err := u.Db.Preload(clause.Associations).First(&user, id).Error; err != nil {
		return nil, err
	}

	return &user, nil
}

func (u User) GetUserByEmail(email string) (*dbModels.User, error) {
	user := &dbModels.User{}
	if err := u.Db.First(user, "email = ?", email).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (u User) ChangeUserAccountStatus(userId uuid.UUID, status dbModels.AccountStatus) (*dbModels.User, error) {
	user := &dbModels.User{}

	err := u.Db.First(user, userId).Update("account_status", status).Error
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (u User) AddFriend(userId, friendId uuid.UUID) error {
	err := u.Db.Debug().Omit("Friends.*").First(&dbModels.User{}, userId).Association("Friends").Append(&dbModels.User{Base:dbModels.Base{Id: friendId}})
	if err != nil {
		return err
	}
	return nil
}

func (u User) RemoveFriend(userId, friendId uuid.UUID) error {
	err := u.Db.Debug().Raw("DELETE FROM user_friends WHERE user_id = ? AND friend_id = ?", userId, friendId).Scan(nil).Error
	if err != nil {
		return err
	}
	return nil
}

func (u User) DeleteUser(id uuid.UUID) (*dbModels.User, error) {
	user := &dbModels.User{}

	err := u.Db.First(user, id).Delete(user).Error
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (u User) IsUserAFriend(userId, friendId uuid.UUID) (bool, error) {
 //user := &dbModels.User{}
//friends := []*dbModels.User{}
	type Result struct {
		UserId uuid.UUID
		FriendId uuid.UUID
	}
	var result Result
	err := u.Db.Debug().Raw("SELECT * FROM user_friends WHERE user_id = ? AND friend_id =? ", userId, friendId).Scan(&result).Error
	//err := u.Db.Debug().Model(&dbModels.User{}).Where("user_id = ? AND friend_id = ?", userId,friendId).Association("Friends").Find(&friends)
	if err == gorm.ErrRecordNotFound {
		return false, nil
	}
	if err != nil {
		return false, err
	}

	logrus.Info(2, result)
	if result.FriendId == uuid.Nil || result.UserId == uuid.Nil {
		return false, nil
	}
	return true, nil
}
