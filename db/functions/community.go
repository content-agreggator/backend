package functions

import (
	"errors"
	"fmt"
	uuid "github.com/satori/go.uuid"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Community struct {
	Db *gorm.DB
}

func (c Community) CreateCommunity(community *dbModels.Community) (*dbModels.Community, error) {
	err := c.Db.Create(community).Error
	if err != nil {
		return nil, err
	}

	return community, nil
}

func (c Community) GetCommunity(id uuid.UUID) (*dbModels.Community, error) {
	var community = &dbModels.Community{}

	err := c.Db.Preload("Users").Preload("Users.User").First(community, id).Error
	if err != nil {
		return nil, err
	}

	return community, nil
}

func (c Community) GetCommunities(after *string, before *string, first *int, last *int) (*[]dbModels.Community, *apiModels.PageInfo, error) {
	communities := []dbModels.Community{}

	if err := c.Db.Scopes(customCursorPaginator(after, before, first, last)).Find(&communities).Error; err != nil {
		return nil, nil, err
	}

	var ids []string
	for _, community := range communities {
		ids = append(ids, community.Id.String())
	}

	info, err := getPageInfo(c.Db, ids, dbModels.Community{})
	if err != nil {
		return nil, nil, err
	}

	return &communities, info, nil
}

func (c Community) AddUserToCommunity(userId, communityId uuid.UUID) (*dbModels.UserCommunity, error) {
	err := c.Db.Where("user_id = ? AND community_id = ?", userId, communityId).
		First(&dbModels.UserCommunity{}).Error
	if errors.Is(err,gorm.ErrRecordNotFound) {
		userCommunity := &dbModels.UserCommunity{
			CommunityId: &communityId,
			UserId:      &userId,
			Role:        dbModels.CommunityRoleUser,
		}

		err = c.Db.Preload(clause.Associations).Create(userCommunity).First(userCommunity).Error
		if err != nil {
			return nil, err
		}

		return userCommunity, nil
	}

	if err != nil  {
		return nil, err
	}



	//err := c.Db.Clauses(clause.OnConflict{
	//	DoNothing: true,
	//	Columns:   []clause.Column{{Name: "community_id"}, {Name: "user_id"}},
	//}).Create(userCommunity).Error
	//if err != nil {
	//	return nil, err
	//}


	return nil, fmt.Errorf("could not join user to community")

}

func (c Community) RemoveUserFromCommunity(userId, communityId uuid.UUID) (*dbModels.UserCommunity, error) {
	userCommunity := &dbModels.UserCommunity{}

	err := c.Db.
		Preload(clause.Associations).
		Where("user_id = ? AND community_id = ?", userId, communityId).First(userCommunity).Delete(userCommunity).Error
	if err != nil {
		return nil, err
	}
	return userCommunity, nil
}

func (c Community) ChangeUserCommunityRole(userId, communityId uuid.UUID, role dbModels.CommunityRole) (*dbModels.UserCommunity, error) {
	var userCommunity *dbModels.UserCommunity

	err := c.Db.Where("user_id = ? AND community_id = ?", userId, communityId).Update("role", role).Error
	if err != nil {
		return nil, err
	}
	return userCommunity, nil
}

func (c Community) IsUserInCommunity(userId, communityId uuid.UUID) (bool, error) {
	err := c.Db.
		Where("user_id = ? AND community_id = ?", userId, communityId).
		First(&dbModels.UserCommunity{}).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil

}
