package main

import (
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	"github.com/rs/cors"
	db "gitlab.com/content-agreggator/backend/db"
	"gitlab.com/content-agreggator/backend/db/functions"
	dbModels "gitlab.com/content-agreggator/backend/db/models"
	"gitlab.com/content-agreggator/backend/graph/generated"
	middlewares "gitlab.com/content-agreggator/backend/graph/middleware"
	apiModels "gitlab.com/content-agreggator/backend/graph/models"
	"gitlab.com/content-agreggator/backend/graph/resolvers"
	"log"
	"net/http"
	"os"
	"time"
)

const defaultPort = "8080"


func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	router := chi.NewRouter()
	router.Use(middlewares.JwtMiddleware())
	router.Use(middlewares.PermissionMiddleware())

	router.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		Debug:            true,
		AllowedHeaders: []string{"*"},
		AllowedMethods: []string{"POST", "GET"},

	}).Handler)
	//router.Use(middlewares.HeadersSetter())
	//router.Use(cors.AllowAll().Handler)

	if  err := db.ConnectDb(); err != nil {
		log.Fatalf("could not connect to database; error = %w", err)
	}

	if err := db.MigrateDb(); err != nil {
		log.Fatalf("could not migrate database; error = %w", err)
	}

	db.DbConnector = &db.DaoGatherer{
		User: functions.User{Db: db.GetDb()},
		Multimedia: functions.Multimedia{Db: db.GetDb()},
		Comment: functions.Comment{Db: db.GetDb()},
		Release: functions.Release{Db: db.GetDb()},
		Community: functions.Community{Db: db.GetDb()},
	}

	config, err := getGqlConfig()
	if err != nil {
		log.Fatalf("could not setup gql config; err = %v", err)
	}

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(*config))

	srv.AddTransport(&transport.Websocket{
		Upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				// Check against your desired domains here
				return true
			},

			ReadBufferSize:  1024,
			WriteBufferSize: 1024,
		},
		KeepAlivePingInterval: 25*time.Second,
	})

	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}

func getGqlConfig() (*generated.Config, error) {
	c := generated.Config{Resolvers: &resolvers.Resolver{}}

	_, err := c.Resolvers.Mutation().AddNovelsFromWln(context.Background(), 1000, 1050)
	if err != nil {
		return nil, err
	}

	c.Directives.HasRole = func(ctx context.Context, obj interface{}, next graphql.Resolver, role apiModels.Role) (interface{}, error) {
		if middlewares.GetAuthFromContext(ctx).HasRole(dbModels.Role(role)) {
			// block calling the next resolver
			return nil, fmt.Errorf("Access denied")
		}

		// or let it pass through
		return next(ctx)
	}

	return &c,nil
}
