package wln

import (
	"bytes"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"gitlab.com/content-agreggator/backend/wln/models"
	"net/http"
	"os"
)

func callWln(values map[string]interface{}) (map[string]interface{}, error) {
	json_data, err := json.Marshal(values)
	if err != nil {
		return nil, err
	}

	resp, err := http.Post(os.Getenv("WLN_API_URL"), "application/json",
		bytes.NewBuffer(json_data))
	if err != nil {
		return nil, err
	}


	var res map[string]interface{}

	err = json.NewDecoder(resp.Body).Decode(&res)
	if err != nil {
		return nil, err
	}

	logrus.Info("WLN call rsp: %v", res)
	return res, nil
}

func LoginToWLN() (map[string]interface{}, error) {

	values := map[string]interface{}{
		"mode":     "do-login",
		"username": os.Getenv("WLN_USERNAME"),
		"password": os.Getenv("WLN_PASSWORD"),
	}

	rsp, err := callWln(values)
	if err != nil {
		return nil, err
	}

	return rsp, nil
}

func GetSeriesId(id int) (*models.Novel,error) {
	values := map[string]interface{}{
		"id":   id,
		"mode": "get-series-id",
	}

	rsp, err := callWln(values)
	if err != nil {
		return nil, err
	}

	jsonString, _ := json.Marshal(rsp["data"])
	novel := models.Novel{}
	err = json.Unmarshal(jsonString, &novel)
	if err != nil {
		return nil, err
	}


	return &novel, nil
}