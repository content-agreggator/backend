package models

type Novel struct {
	AlternateNames []string `json:"alternatenames,omitempty"`
	Authors        []struct {
		Author string `json:"author,omitempty"`
		Id     int    `json:"id,omitempty"`
	} `json:"authors,omitempty"`
	Covers []struct {
		Chapter     string  `json:"chapter,omitempty"`
		Description string  `json:"description,omitempty"`
		Id          int     `json:"id,omitempty"`
		SrcfName    string  `json:"srcfname,omitempty"`
		Url         string  `json:"url,omitempty"`
		Volume      float32 `json:"volume,omitempty"`
	} `json:"covers,omitempty"`
	Demographic string `json:"demographic,omitempty"`
	Description string `json:"description,omitempty"`
	Genres      []struct {
		Genre string `json:"genre,omitempty"`
		Id    int    `json:"id,omitempty"`
	} `json:"genres,omitempty"`
	Id           int `json:"id,omitempty"`
	Illustrators []struct {
		Id           int    `json:"id,omitempty"`
		Illustrators string `json:"illustrators,omitempty"`
	} `json:"illustrators,omitempty"`
	Latest struct {
		Chp float32 `json:"chp,omitempty"`
		Frg float32 `json:"frg,omitempty"`
		Vol float32 `json:"vol,omitempty"`
	} `json:"latest"`
	LatestPublished string `json:"latest_published,omitempty"`
	LatestStr       string `json:"latest_str,omitempty"`
	LicenseEn       bool   `json:"license_en,omitempty"`
	MostRecent      string `json:"most_recent,omitempty"`
	OrigLang        string `json:"orig_lang,omitempty"`
	OrigStatus      string `json:"orig_status,omitempty"`
	Progress        struct {
		Chp float32 `json:"chp,omitempty"`
		Frg float32 `json:"frg,omitempty"`
		Vol float32 `json:"vol,omitempty"`
	} `json:"progress"`
	PubDate    string `json:"pub_date,omitempty"`
	Publishers []struct {
		Id        int    `json:"id,omitempty"`
		Publisher string `json:"publisher,omitempty"`
	} `json:"publishers,omitempty"`
	Rating struct {
		Avg  float32 `json:"avg,omitempty"`
		Num  int     `json:"num,omitempty"`
		User int     `json:"user,omitempty"`
	} `json:"rating"`
	RatingCount int    `json:"rating_count,omitempty"`
	Region      string `json:"region,omitempty"`
	Releases    []struct {
		Chapter   float32 `json:"chapter,omitempty"`
		Fragment  interface{}  `json:"fragment,omitempty"`
		Postfix   string  `json:"postfix,omitempty"`
		Published string  `json:"published,omitempty"`
		Series    struct {
			Id     int    `json:"id,omitempty"`
			Name   string `json:"name,omitempty"`
		} `json:"series"`
		Srcurl string `json:"srcurl,omitempty"`
		TlGroup struct {
			Id   int    `json:"id,omitempty"`
			Name string `json:"name,omitempty"`
		} `json:"tlgroup"`
		Volume float32 `json:"volume,omitempty"`
	} `json:"releases,omitempty"`
	SimilarSeries []struct {
		Id    int    `json:"id,omitempty"`
		Title string `json:"title,omitempty"`
	} `json:"similar_series,omitempty"`
	Tags []struct {
		Id  int    `json:"id,omitempty"`
		Tag string `json:"tag,omitempty"`
	} `json:"tags,omitempty"`
	Title        string `json:"title,omitempty"`
	TyType       string `json:"ty_type,omitempty"`
	TotalWatches int    `json:"total_watches,omitempty"`
	Type         string `json:"type,omitempty"`
	Watch        bool   `json:"watch,omitempty"`
	WatchLists   bool   `json:"watchlists,omitempty"`
	Website      string `json:"website,omitempty"`
}
